// actua como emisor de eventos para todos los componentes

const eventBus = {}

//
eventBus.install = function (Vue) {
    Vue.prototype.$bus = new Vue()
}

export default eventBus