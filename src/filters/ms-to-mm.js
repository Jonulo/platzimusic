// duración de las canciones de milisegundos a minutos

const msToMm = {}

function converMsToMm (ms){
    const min = Math.floor(ms/60000)
    const sec = ((ms % 60000 / 1000).toFixed()) //fixed redondea a enteros

    return `${min}:${sec}`
}

msToMm.install = function(Vue){ //install para installar el objeto de forma GLOBAL!
    Vue.filter('ms-to-mm' ,(val) => {  //creamos el filtro y dentro declaramos 2 paramateros, el primero es le nombrey la segunda es la funcion que devuelve un valor
        return converMsToMm(val)
        
    }) 
}

export default msToMm //ahora, para instalarlo y usarlos global lo importamos en main.js