import Vue from 'vue'
import VueRouter from 'vue-router'
import App from '@/App.vue'

import routes from '@/routes' //routes contiene array de la rutas decalaradas en routes.js

import eventBus from '@/plugins/event-bus'
import vuetify from '@/plugins/vuetify'

import msToMm from '@/filters/ms-to-mm'
import blur from '@/directives/blur'

Vue.use(VueRouter)
Vue.use(eventBus) //le decimos a vue que use este plugin de forma GLOBAL

Vue.use(msToMm) //instalado el filtro! ahora ya se puede aplicar en los componentes!
Vue.use(blur) // instalamos la DIRECTIVA PERSONALIZADA  de manera global | IMPORTANTE se aplica en el componente así: "v-blur"

const router = new VueRouter({
  routes: routes,
  mode: 'history' // para quitar el # de la URL
})

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
  vuetify
})
