//desenfocar las partes del dom que queramos

const blur = {}

//etapas de las directivas
// 1. bind es como el mounted del componente
//2. update
//3. unbind.. updated ¿?

function setBlur(el, binding){ // "el" es el elemento html "binding" tiene info sobre el valor que se le asigna a la directiva, tercero refiere al nodo del vDOM, el cuarto es una ref al nodo anterior del vDOM
    el.style.filter = !binding.value ? 'blur(2px)' : 'none'
    el.style.cursor = !binding.value ? 'not-allowed' : 'inherit' //inherit es heredarlo del padre
    //recorrer elementos para deshabilitar los botones de playsong
    //queryselectorall nos devuelves los tags del dom que son hijos del elemento que le dijimos
    el.querySelectorAll('button').forEach( span => {
        if(!binding.value){
            span.style.cursor = 'not-allowed'
        }else{
            span.removeAttribute('disabled')
        }
    })
}

blur.install = function (Vue){
    Vue.directive('blur', {
        bind(el, binding){
            setBlur(el, binding)
        }
    })
}

export default blur