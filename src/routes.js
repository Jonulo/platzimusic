import search from '@/components/search.vue'
import about from '@/components/about.vue'
import TrackDetail from '@/components/TrackDetail.vue'
import prueba from '@/components/vuety.vue'

const routes = [
    {
        path: '/',
        component: search,
        name: 'search'
    },
    {
        path: '/about',
        component: about,
        name: 'about'
    },
    {
        path: '/track/:id',
        component: TrackDetail,
        name: 'track'
    },
    {
        path: '/vuety',
        component: prueba,
        name: 'vuety'
    }
]

export default routes