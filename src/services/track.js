import platziMusicService from './platzi-music'

const trackService  = {}

//metodo para iteractuar con platzimusic
trackService.search = function (q) { //"q" es de query y se respeta la nomenclatura por tradicion y ES EL PARAMETRO QUE PIDE EL USUARIO

    const type = 'track' //esto es por que la api permite buscar por diferente contenido (artistas,canciones..)entonces solo buscaremos track

    //platimusic usa trae - trae usa Fetch - y fetch usa promises de JS
    //promises usa then y catch para saber cuando se va a resolver y cuando va a fallar

    //usaremos los metodos de TRAE para las peticiones (documentacion de TRAE)

    //PlatziMusicService ya trae la URL que asignamos en config, entonces GET como primer parametro pasamos partes especificas de la url
    //Segundo parametro es un objeto que recibe otro obj "params" estos parametros viajan en la url como metodos de busqueda
    return platziMusicService.get('/search', {
        params: { q: q, type: type} //objeto y variables iguales solo escibe una {q, type}
    }) 
    .then( (res) => {return res.data})  //devuelve la data de la respuesta
}

 trackService.getById = function (id){

     return platziMusicService.get(`/tracks/${id}`)
        .then(res => res.data)
 }


export default trackService